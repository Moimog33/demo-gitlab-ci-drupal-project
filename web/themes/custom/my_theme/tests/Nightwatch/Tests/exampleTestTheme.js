// Dummy simple test.
module.exports = {
  "@tags": ["my_module", "demo"],
  before(browser) {
    browser.drupalInstall({
      setupFile: "core/tests/Drupal/TestSite/TestSiteInstallTestScript.php",
      // setupFile: "core/tests/Drupal/TestSite/TestSiteOliveroInstallTestScript.php",
      installProfile: "minimal",
    });
    browser.setWindowSize(1600, 800);
  },
  after(browser) {
    browser.drupalUninstall();
  },
  "Demo test theme": (browser) => {
    browser
      .drupalRelativeURL("/")
      .waitForElementVisible("body", 1000)
      .assert.elementPresent("body")
      .assert.textContains("body", "Log in")
      .saveScreenshot(`${browser.screenshotsPath}/Desktop_test.jpg`)
      .resizeWindow(375, 812)
      .saveScreenshot(`${browser.screenshotsPath}/Mobile_test.jpg`)
      .drupalLogAndEnd({ onlyOnError: false });
  },
};
